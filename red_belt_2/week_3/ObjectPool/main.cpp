#include "test_runner.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <queue>
#include <stdexcept>
#include <set>
using namespace std;

template <class T>
class ObjectPool {
public:
  T* Allocate(){
	  if(!free.empty()){
		  T* ukaz = free.front();
		  all.insert(ukaz);
		  free.pop();
		  return ukaz;
	  }else{
		  T* ukaz = new T;
		  all.insert(ukaz);
		  return ukaz;
	  }
  }
  T* TryAllocate(){
	  if(!free.empty()){
	  	T* ukaz = free.front();
	  	all.insert(ukaz);
	  	free.pop();
	  	return ukaz;
	  }else{
		 return nullptr;
	  }
  }

  void Deallocate(T* object){
	  if (all.find(object)==all.end()){
		  throw invalid_argument("The element is not contained in the pool of objects");
	  }
	  free.push(object);
	  all.erase(object);
  }

  ~ObjectPool(){
	  for (T* elem:all){
		  delete elem;
	  }
	  all.clear();

	  while(!free.empty()){
		  delete free.front();
		  free.pop();
	  }
  }

private:
  // Добавьте сюда поля
  queue<T*> free ;
  set<T*> all;

};


void TestObjectPool() {
  ObjectPool<string> pool;

  auto p1 = pool.Allocate();
  auto p2 = pool.Allocate();
  auto p3 = pool.Allocate();

  *p1 = "first";
  *p2 = "second";
  *p3 = "third";

  pool.Deallocate(p2);
  ASSERT_EQUAL(*pool.Allocate(), "second");

  pool.Deallocate(p3);
  pool.Deallocate(p1);
  ASSERT_EQUAL(*pool.Allocate(), "third");
  ASSERT_EQUAL(*pool.Allocate(), "first");

  pool.Deallocate(p1);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestObjectPool);
  return 0;
}
