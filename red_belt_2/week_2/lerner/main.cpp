#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <sstream>
using namespace std;


class Learner {
 private:
  set<string> dict;

 public:
  int Learn(const vector<string>& words) {
    int s = dict.size();
    for (auto& word: words){
    	dict.insert(word);
    }
    return dict.size()-s;
  }

  vector<string> KnownWords() {
	vector<string> output(dict.begin(), dict.end());
	return output;
  }
};

int main() {
  Learner learner;
  string line;
  int i = 0;
  while (getline(cin, line)) {
    vector<string> words;
    stringstream ss(line);
    string word;
    while (ss >> word) {
      words.push_back(word);
    }
    cout << learner.Learn(words) << "\n";
    i++;
    if (i==3){
    	break;
    }
  }
  cout << "=== known words ===\n";
  for (auto word : learner.KnownWords()) {
    cout << word << "\n";
  }
}
