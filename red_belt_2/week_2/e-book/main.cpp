#include <iomanip>
#include <iostream>
#include <vector>
#include <utility>
#include <map>
//#include "test_runner.h"
using namespace std;

class ReadingManager {
public:
  ReadingManager(){
	  vector<double> k(1001, 0);
	  rating = k;
  }

  void Read(int user_id, int page_count){
	  if(users.find(user_id)==users.end()){
		  users[user_id] = page_count;
		  pages[page_count] +=1;
	  }else{
		  pages[users[user_id]]-=1;
		  users[user_id] = page_count;
		  pages[page_count] +=1;
	  }
	  int less = 0;
	  for (auto page:pages){
		  rating[page.first] = less;
		  less+=page.second;
	  }
  }

  double Cheer(int user_id) {
	  if(users.find(user_id)==users.end()){
		  return 0;
	  }else if(users.size()== 1){
		  return 1;
	  }else{
		  return 1.0*rating[users[user_id]]/(users.size()-1);
	  }
  }

private:
  map<int, int> pages;
  map<int, int> users;
  vector<double> rating;
};





int main() {
  // Для ускорения чтения данных отключается синхронизация
  // cin и cout с stdio,
  // а также выполняется отвязка cin от cout
  ios::sync_with_stdio(false);
  cin.tie(nullptr);

  ReadingManager manager;

  int query_count;
  cin >> query_count;

  for (int query_id = 0; query_id < query_count; ++query_id) {
    string query_type;
    cin >> query_type;
    int user_id;
    cin >> user_id;

    if (query_type == "READ") {
      int page_count;
      cin >> page_count;
      manager.Read(user_id, page_count);
    } else if (query_type == "CHEER") {
      cout << setprecision(6) << manager.Cheer(user_id) << "\n";
    }
  }

  return 0;
}




