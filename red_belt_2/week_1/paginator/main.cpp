#include "test_runner.h"
#include <iterator>
#include <numeric>
#include <iostream>
#include <vector>
#include <string>
using namespace std;

// Реализуйте шаблон класса Paginator


template<typename Iterator>
struct SinglePage {
	Iterator first, last;

	Iterator begin()const{
		return first;
	}
	Iterator end()const{
		return last;
	}
	size_t size() const {
		return last-first;
	}
};

template <typename Iterator>
class Paginator {
public:
	Paginator(Iterator f, Iterator l, size_t ps)
			:first(f),last(l),page_size(ps){
		for (size_t i = 1; i<= size(); i++){
			if(i!=size()){
				SinglePage<Iterator> sp = {first+(i-1)*ps, first+i*ps};
				book.push_back(sp);
			}else{
				SinglePage<Iterator> sp = {first+(i-1)*ps, last};
				book.push_back(sp);
			}
		}
	}

	size_t size() const {
		if ((last-first)%page_size >0){
			return (last-first)/page_size +1;
		}else{
			return (last-first)/page_size;
		}
	}

	typename vector<SinglePage<Iterator>>::iterator begin(){
		return book.begin();
	}

	typename vector<SinglePage<Iterator>>::iterator end(){
		return book.end();
	}

private:
	Iterator first, last;
	size_t page_size;
	vector<SinglePage<Iterator>> book;
};

template <typename C>
auto Paginate(C& c, size_t page_size) {
	return Paginator(c.begin(), c.end(), page_size);
}

void TestLooping() {
  vector<int> v(15);
  iota(v.begin(), v.end(), 1);

  Paginator<vector<int>::iterator> paginate_v(v.begin(), v.end(), 6);
  ostringstream os;
  for (const auto& page : paginate_v) {
    for (int x : page) {
      os << x << ' ';
    }
    os << '\n';
  }
  ASSERT_EQUAL(os.str(), "1 2 3 4 5 6 \n7 8 9 10 11 12 \n13 14 15 \n");
}

void TestPageCounts() {
  vector<int> v(15);

  ASSERT_EQUAL(Paginate(v, 1).size(), v.size());
  ASSERT_EQUAL(Paginate(v, 3).size(), 5u);
  ASSERT_EQUAL(Paginate(v, 5).size(), 3u);
  ASSERT_EQUAL(Paginate(v, 4).size(), 4u);
  ASSERT_EQUAL(Paginate(v, 15).size(), 1u);
  ASSERT_EQUAL(Paginate(v, 150).size(), 1u);
  ASSERT_EQUAL(Paginate(v, 14).size(), 2u);
}

void TestPaginatorIteration(){
	vector<int> k = {1,2,3,4,5,6,7,8};
	vector<int> result;
	Paginator<vector<int>::iterator> pp = {k.begin(), k.end(), 3};
	for (auto elem:pp){
		for (auto part: elem){
			result.push_back(part);
		}
	}
	ASSERT_EQUAL(result, k);
}

void TestPaginatorSize(){
	vector<int> k = {1,2,3,4,5,6,7,8};
	Paginator<vector<int>::iterator> pp = {k.begin(), k.end(), 3};
	ASSERT_EQUAL(pp.size(), 3);

	vector<int> m= {1,2,3,4,5,6,7,8,9};
	Paginator<vector<int>::iterator> pm = {m.begin(), m.end(), 3};
	ASSERT_EQUAL(pm.size(), 3);

	string stroka = "abcd ";
	Paginator<string::iterator> st = {stroka.begin(),stroka.end(),3};

}
void TestSinglePage(){
	vector<int> k = {1,2,3};
	SinglePage<vector<int>::iterator> sp = {k.begin(),k.end()};
	vector<int> result;
	for (auto elem:sp){
		result.push_back(elem);
	}
	ASSERT_EQUAL(result, k);

	string stroka = "abs";
	SinglePage<string::iterator> st = {stroka.begin(),stroka.end()};
	vector<char> result2;
	for (auto elem:st){
		result2.push_back(elem);
	}
	ASSERT_EQUAL(result2[0], 'a');
	ASSERT_EQUAL(result2[1], 'b');
	ASSERT_EQUAL(result2[2], 's');
}

void TestSinglePageSize(){
	vector<int> k = {1,2,3,4,5};
	SinglePage<vector<int>::iterator> sp = {k.begin(),k.end()};
	ASSERT_EQUAL(sp.size(), 5);

	string stroka = "absaw";
	SinglePage<string::iterator> st = {stroka.begin(),stroka.end()};
	ASSERT_EQUAL(st.size(), 5);
}

void TestModification() {
  vector<string> vs = {"one", "two", "three", "four", "five"};
  for (auto page : Paginate(vs, 2)) {
    for (auto& word : page) {
      word[0] = toupper(word[0]);
    }
  }

  const vector<string> expected = {"One", "Two", "Three", "Four", "Five"};
  ASSERT_EQUAL(vs, expected);
}

void TestPageSizes() {
  string letters(26, ' ');

  Paginator<string::iterator> letters_pagination(letters.begin(), letters.end(), 11);
  vector<size_t> page_sizes;
  for (const auto& page : letters_pagination) {
    page_sizes.push_back(page.size());
  }

  const vector<size_t> expected = {11, 11, 4};
  ASSERT_EQUAL(page_sizes, expected);
}

void TestConstContainer() {
  const string letters = "abcdefghijklmnopqrstuvwxyz";

  vector<string> pages;
  for (const auto& page : Paginate(letters, 10)) {
    pages.push_back(string(page.begin(), page.end()));
  }

  const vector<string> expected = {"abcdefghij", "klmnopqrst", "uvwxyz"};
  ASSERT_EQUAL(pages, expected);
}


int main(){
	TestRunner tr;
	RUN_TEST(tr, TestSinglePage);
	RUN_TEST(tr, TestSinglePageSize);
	RUN_TEST(tr, TestPaginatorSize);
	RUN_TEST(tr, TestPaginatorIteration);
	RUN_TEST(tr, TestPageCounts);
	RUN_TEST(tr, TestLooping);
	RUN_TEST(tr, TestModification);
	RUN_TEST(tr, TestPageSizes);
	RUN_TEST(tr, TestConstContainer);
}






