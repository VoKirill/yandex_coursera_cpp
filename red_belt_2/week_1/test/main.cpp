#include <iostream>
#include <iterator>
#include <vector>
using namespace std;

template<typename Iterator>
struct IteratorRange {
	Iterator first, last;

	Iterator begin()const{
		return first;
	}

	Iterator end()const{return last;
	}

	size_t size() const {
		return last-first;
	}
};

int main(){
	vector<int> k = {1,2,3,88};
	IteratorRange<vector<int>::iterator> sp = {k.begin(), k.end()};
	for (auto elem: sp){
		cout<<elem;
	}
	cout<<sp.size();
	return 0;
}
