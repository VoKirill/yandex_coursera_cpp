#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;




template <typename RandomIt>
void MergeSort_2(RandomIt range_begin, RandomIt range_end) {
    if (range_end - range_begin < 2) {
        return;
    }
    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    auto it = begin(elements) + elements.size() / 2;
    MergeSort(begin(elements), it);
    MergeSort(it, end(elements));
    merge(begin(elements), it, it, end(elements), range_begin);
}

template <typename RandomIt>
void MergeSort_3(RandomIt range_begin, RandomIt range_end) {
    if (range_end - range_begin < 3) {
        return;
    }
    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    auto it1 = begin(elements) + elements.size() / 3;
    auto it2 = it1 + elements.size() / 3;

    MergeSort(begin(elements), it1);
    MergeSort(it1, it2);
    MergeSort(it2, end(elements));

    std::vector<typename RandomIt::value_type> tmp;

    merge(begin(elements), it1, it1, it2, back_inserter(tmp));
    merge(tmp.begin(), tmp.end(), it2, end(elements), range_begin);
}

int main() {
    vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1, 5};
    MergeSort(begin(v), end(v));
    for (int x : v) {
        cout << x << " ";
    }
    cout << endl;
    return 0;
}

