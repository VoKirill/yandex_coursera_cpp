#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
using namespace std;


template <typename RandomIt>
pair<RandomIt, RandomIt> FindStartsWith(RandomIt range_begin, RandomIt range_end, const string& prefix){
	string to_change = prefix;
	auto left = lower_bound(range_begin, range_end, to_change );
	++to_change[to_change.size()-1];
	auto right = lower_bound(range_begin, range_end, to_change);
	return make_pair(left, right);

}

//[to_change](string& a, string& b){return a.substr(0, to_change.size())<b.substr(0, to_change.size());}


int main() {
  const vector<string> sorted_strings = {"moscow", "motovilikha", "murmansk"};

  const auto mo_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), "mo");
  for (auto it = mo_result.first; it != mo_result.second; ++it) {
    cout << *it << " ";
  }
  cout << endl;

  const auto mt_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), "mt");
  cout << (mt_result.first - begin(sorted_strings)) << " " <<
      (mt_result.second - begin(sorted_strings)) << endl;

  const auto na_result =
      FindStartsWith(begin(sorted_strings), end(sorted_strings), "na");
  cout << (na_result.first - begin(sorted_strings)) << " " <<
      (na_result.second - begin(sorted_strings)) << endl;

  return 0;
}
