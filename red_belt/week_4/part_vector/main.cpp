/*
 * main.cpp
 *
 *  Created on: 08.03.2019
 *      Author: user
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


void PrintVectorPart(const vector<int>& numbers){
	auto neg_it = find_if(numbers.begin(),numbers.end(),
			[](int number){return number<0;});
	for (auto it = neg_it; it!=numbers.begin();){
		cout << *(--it)<<' ';
	}
}

int main() {
  vector<int> k = {6, 1, 8, -5, 4}, m = {-6, 1, 8, -5, 4}, c={6, 1, 8, 5, 4};
  PrintVectorPart(k);
  cout << endl;
  PrintVectorPart(m);  // ничего не выведется
  cout << endl;
  PrintVectorPart(c);
  cout << endl;
  return 0;
}
