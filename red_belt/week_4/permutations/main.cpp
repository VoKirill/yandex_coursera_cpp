#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

void print(vector<int>& vec){
	for (auto& elem:vec){
		cout<<elem<< ' ';
	}
}

int main(){
	int k;
	cin >> k;
	vector<int> vec;
	for (int i = 1; i<=k; ++i){
		vec.push_back(i);
	}
	sort(vec.rbegin(), vec.rend());
	print(vec);
	cout<<endl;
	while(prev_permutation(vec.begin(), vec.end())){
		print(vec);
		cout<<endl;
	}
	return 0;
}

