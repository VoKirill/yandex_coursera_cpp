#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>

using namespace std;
/* first_task
int main(){

	string start;
	int amount;

	cin>> start;
	cin >> amount;

	deque<string> deq = {start};
	string buf;

	for (int i=0; i<=amount; i++){

		getline(cin, buf);
		if (i!=0){
			deq.push_back(" ");
		}
		deq.push_back(buf);
		if (i!= amount){
			deq.push_back(")");
			deq.push_front("(");
		}
	}
	for(auto& elem: deq){
		cout<<elem;
	}

	return 0;
}
*/
int main(){

	string start;
	int amount;

	cin>> start;
	cin >> amount;

	deque<string> deq = {start};
	deque<char> znaki;
	string buf;
	vector<char> elder = {'/','*'}, young = {'+','-'};

	for (int i=0; i<=amount; i++){

		getline(cin, buf);
		znaki.push_back(buf[0]);
		if(znaki.size()>=2 && (znaki[znaki.size()-1]=='/'||znaki[znaki.size()-1]=='*')&&(znaki[znaki.size()-2]=='+'||znaki[znaki.size()-2]=='-')){
			deq.push_back(")");
			deq.push_front("(");
		}
		if (i!=0){
			deq.push_back(" ");
		}
		deq.push_back(buf);
	}
	for(auto& elem: deq){
		cout<<elem;
	}

	return 0;
}














