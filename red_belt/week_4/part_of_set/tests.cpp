#include "tests.h"
#include <vector>
using namespace std;

void TestNum(){
	{
	vector<int> k = {4,5};
	set<int> m = {1,2,3,4,5};
	int border =3;
	vector<int> result = FindGreaterElements(m, border);
	AssertEqual(result, k, "int_");
	}

}
void TestString(){
	vector<string> k = {"AD", "B", "C"};
	set<string> m = {"AA", "AD", "B", "C"};
	string border = "AB";
	vector<string> result = FindGreaterElements(m, border);
	vector<string> compare = {"AD", "B", "C"};
	AssertEqual(result, compare, "string_test");
}

void TestAll(){
	TestRunner r;
	r.RunTest(TestNum,"int_");
	r.RunTest(TestString,"string_test");

}









