
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

template <typename T>
vector<T> FindGreaterElements(const set<T>& elements, const T& border){
	auto big_it = find_if(elements.begin(), elements.end(),
			[border](T elem){return elem > border;});
	vector<T> answer;
	for (auto it = big_it; it!= elements.end();++it){
		answer.push_back(*it);
	}
	return answer;
}
