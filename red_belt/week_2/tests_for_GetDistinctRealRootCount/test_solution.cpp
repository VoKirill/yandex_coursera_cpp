#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

/*
int ANotZerro(double a, double b, double c){
	double D = b*b-4*a*c;
	if (D>0){
		return 2;
	}else if (D == 0){
		return 1;
	}else{
		return 0;
	}
}

int AIsZerro(double b, double c){
	if (b!=0){
		return 1;
	}else {
		return 0;
	}
}

int GetDistinctRealRootCount(double a, double b, double c) {
  // Вы можете вставлять сюда различные реализации функции,
  // чтобы проверить, что ваши тесты пропускают корректный код
  // и ловят некорректный
	if (a!= 0){
		ANotZerro(a, b, c);
	}else{
		AIsZerro(b,c);
	}
}
*/

void DIsZero(){
	AssertEqual(GetDistinctRealRootCount(2, 4, 2), 1);
}

void DLessZero(){
	AssertEqual(GetDistinctRealRootCount(2, 2, 2), 0);
}
void DMoreZero(){
	AssertEqual(GetDistinctRealRootCount(2, 6, 2), 2);
}
void AIsZero(){
	AssertEqual(GetDistinctRealRootCount(0, 6, 2), 1);
}
void Const(){
	AssertEqual(GetDistinctRealRootCount(0, 0, 2), 0);
}

int main() {
  TestRunner runner;
  // добавьте сюда свои тесты
  runner.RunTest(DIsZero, "DIszerro");
  runner.RunTest(DLessZero, "DLesszerro");
  runner.RunTest(DMoreZero, "DMorezerro");
  runner.RunTest(AIsZero, "AisZerro");
  runner.RunTest(Const, "const");
  return 0;
}


