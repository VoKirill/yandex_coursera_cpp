#include <iostream>
#include <string>

using namespace std;

class Animal {
public:
    Animal(const string& t = "animal" )
		:Name(t){}

    const string Name;
};


class Dog: public Animal {
public:
    // ваш код
	Dog (const string& dog_name):Animal(dog_name){}
    void Bark() {
        cout << Name << " barks: woof!" << endl;
    }
};

int main(){
	return 0;
}
