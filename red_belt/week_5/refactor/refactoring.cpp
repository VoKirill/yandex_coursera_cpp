#include <iostream>
#include <string>
#include <vector>

using namespace std;


class Person {
public:
    Person(const string& n, const string& p) : name(n), profession(p) {}

    string GetName() const {
        return name;
    }
    string GetProfession() const {
        return profession;
    }
    virtual void Walk(const string& destination) const = 0;

private:
    const string name;
    const string profession;
};


class Student : public Person {
public:

    Student(const string& n, const string& fs) : Person(n, "Student"), favouritesong(fs) {}

    void Learn() const {
        cout << "Student: " << GetName() << " learns" << endl;
    }

    void Walk(const string& destination) const override {
        cout << "Student: " << GetName() << " walks to: " << destination << endl;
        SingSong();
    }

    void SingSong() const {
        cout << "Student: " << GetName() << " sings a song: " << favouritesong << endl;
    }

private:
    const string favouritesong;
};


class Teacher : public Person {
public:

    Teacher(const string& n, const string& s) : Person(n, "Teacher"), subject(s) {}

    void Teach() const {
        cout << "Teacher: " << GetName() << " teaches: " << subject << endl;
    }

    void Walk(const string& destination) const override {
        cout << "Teacher: " << GetName() << " walks to: " << destination << endl;
    }

private:
    const string subject;
};


class Policeman : public Person {
public:
    Policeman(const string& n) : Person(n, "Policeman") {}

    void Check(Person& person) const {
        cout << "Policeman: " << GetName() << " checks " << person.GetProfession() << ". "
             << person.GetProfession() << "'s name is: " << person.GetName() << endl;
    }

    void Walk(const string& destination) const override {
        cout << "Policeman: " << GetName() << " walks to: " << destination << endl;
    }
};


void VisitPlaces(Person& person, vector<string> places) {
    for (const auto& p : places) {
        person.Walk(p);
    }
}


int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}
