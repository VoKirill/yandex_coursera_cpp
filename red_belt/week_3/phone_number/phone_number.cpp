#include "phone_number.h"
#include <iostream>
#include <sstream>
#include <string>
#include <exception>

using namespace std;

//PhoneNumeb

PhoneNumber::PhoneNumber(const string &international_number){
	if (international_number[0]!= '+'){
		stringstream ss;
		ss<< "wrong_plus";
		throw invalid_argument(ss.str());
	}else{
		string sub = international_number.substr(1,international_number.size()-1);
		stringstream input(sub);
		string field;
		if (getline(input, field, '-')){
			country_code_ = field;
		}else{
			stringstream ss;
			ss<< "wrong_country_code";
			throw invalid_argument(ss.str());
		}
		if (getline(input, field, '-')){
			city_code_ = field;
		}else{
			stringstream ss;
			ss<< "wrong_city_code";

			throw invalid_argument(ss.str());
		}

		if (getline(input, field)){
			local_number_ = field;
		}else{
			stringstream ss;
			ss<< "wrong_local";
			throw invalid_argument(ss.str());
		}
	}
}


string PhoneNumber::GetCountryCode() const{
	return country_code_;
}

string PhoneNumber::GetCityCode() const{
	return city_code_;
}

string PhoneNumber::GetLocalNumber() const{
	return local_number_;
}

string PhoneNumber::GetInternationalNumber() const{
	return "+"+country_code_+"-"+city_code_+"-"+local_number_;
}



















