#include "tests.h"

using namespace std;
void Testexception(){
	try{
		PhoneNumber phone("+7-925-7286342");
	}catch(exception& ex){
		cout<<ex.what();
	}
}
void TestCountryCode(){
	{
	PhoneNumber phone("+7-925-7286342");
	AssertEqual(phone.GetCountryCode(), "7", "compare_country_code");
	}
}

void TestCityCode(){
	PhoneNumber phone("+7-925-7286342");
	AssertEqual(phone.GetCityCode(), "925", "compare_country_code");

}

void TestLocalNumber(){
	{
	PhoneNumber phone("+7-925-7286342");
	AssertEqual(phone.GetLocalNumber(), "7286342", "compare_local");
	}
	{
	PhoneNumber phone("+7-925-728-63-42");
	AssertEqual(phone.GetLocalNumber(), "728-63-42", "multy_local");
	}
}


void TestAll(){
	TestRunner r;
	r.RunTest(Testexception,"exception_problem");
	r.RunTest(TestLocalNumber, "local_number");
	r.RunTest(TestCityCode, "city_code");
	r.RunTest(TestCountryCode,"country_code");
}










