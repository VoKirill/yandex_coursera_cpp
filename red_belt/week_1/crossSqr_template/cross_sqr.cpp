/*
 * cross_sqr.cpp
 *
 *  Created on: 25.02.2019
 *      Author: user
 */

#include <iostream>
#include <exception>
#include <vector>
#include <map>
#include <utility>
using namespace std;

// Предварительное объявление шаблонных функций
template<typename T> T Sqr( T& x);
template <typename tip> vector<tip> Sqr(vector<tip>& X);
template <typename t, typename p> pair<t,p> Sqr(pair<t,p>& x);
template <typename t, typename p> map<t,p> Sqr(map<t,p>& x);
//template<typename T> void FuncB(T x);

template<typename T>
T Sqr( T& x){
	return x*x;
}

template <typename tip>
vector<tip> Sqr(vector<tip>& X){
	//tip k;
	for (int i = 0; i<X.size(); i++){
		X[i] = Sqr(X[i]);
	}
	return X;
}

template <typename t, typename p>
pair<t,p> Sqr(pair<t,p>& x){
	x.first = Sqr(x.first);
	x.second = Sqr(x.second);
	return x;
}

template <typename t, typename p>
map<t,p> Sqr(map<t,p>& x){
	for (auto& elem: x){
		elem.second = Sqr(elem.second);
	}
	return x;
}


int main(){
	// Пример вызова функции
	vector<int> v = {1, 2, 3};
	cout << "vector:";
	for (int x : Sqr(v)) {
	  cout << ' ' << x;
	}
	cout << endl;

	map<int, pair<int, int>> map_of_pairs = {
	  {4, {2, 2}},
	  {7, {4, 3}}
	};
	cout << "map of pairs:" << endl;
	for (const auto& x : Sqr(map_of_pairs)) {
	  cout << x.first << ' ' << x.second.first << ' ' << x.second.second << endl;
	}

	return 0;
}


