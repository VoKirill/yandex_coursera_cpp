/*
 * main.cpp
 *
 *  Created on: 18 февр. 2019 г.
 *      Author: k.vokhmintcev
 */

#include <iostream>
#include <vector>
#include <cstdint>
using namespace std;




int main(){
	int N;
	int64_t value;
	vector<int64_t> temp;
	double mid_temp = 0;
	cin >> N;
	for (int i = 0; i<N; ++i){
		cin>>value;
		temp.push_back(value);
		mid_temp += value;
	}
	mid_temp = 1.0*mid_temp/static_cast<int>(temp.size());
	int amount = 0;
	vector<int> position;
	for (int i = 0;  i<temp.size(); ++i){
		if (temp[i]>mid_temp){
			amount+=1;
			position.push_back(i);
		}
	}
	cout<<amount<<endl;
	for (auto& elem: position){
		cout <<  elem<< " ";
	}
	return 0;
}


