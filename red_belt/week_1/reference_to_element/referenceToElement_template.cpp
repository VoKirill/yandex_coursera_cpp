/*
 * referenceToElement_template.cpp
 *
 *  Created on: 26 февр. 2019 г.
 *      Author: k.vokhmintcev
 */

#include <iostream>
#include <map>
#include <exception>
#include <string>
using namespace std;

template <typename key, typename value>
value& GetRefStrict(map<key,value>& x, key k){
	//exception runtime_error;
	for (auto& elem: x){
		if (elem.first ==  k){
			value& result = x[k];
			return result;
		}
	}
	throw runtime_error("");
}

int main(){
	map<int, string> m = {{0, "value"}};
	string& item = GetRefStrict(m, 0);
	item = "newvalue";
	cout << m[0] << endl;
	return 0;
}

